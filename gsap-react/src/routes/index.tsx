import Rinkel from "../pages/cases/Rinkel";
import CasesAndProces from "../pages/CasesAndProces";
import Home from "../pages/Home";

export const routes = [
  {
    path: '/',
    element: <Home />
  },
  {
    path: 'cases-en-werkwijze',
    children: [
      {
        path: '/',
        element: <CasesAndProces />
      },
      {
        path: '/rinkel',
        element: <Rinkel />
      }
    ]
  }
]