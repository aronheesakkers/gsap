import { Unit, UnitConvert } from "./unit";

/**
 * Layout division unit in pixels
 */
export const LAYOUT_UNIT = 16;

export const theme = Object.freeze({
	LAYOUT_PADDING: '0 10em',
	colors: {
		transparent: "transparent",
		primary: {
			get default() {
				return this[600];
			},
			lightest: "#fff5f9",
			600: "#FE5143",
		},
		secondary: {
			get default() {
				return this[700];
			},
			lighter: "#746e9a",
			lightest: "#e9e8ef",
			stroke: "#d7d7e0",
			50: "#edebff",
			100: "#c9c6ed",
			200: "#a4a1de",
			300: "#807bd0",
			400: "#5d55c1",
			500: "#443ca8",
			600: "#342e83",
			700: "#262261",
			800: "#15143b",
			900: "#060619",
		},
		tertiary: {
			get default() {
				return this[500]
			},
			500: '#F1F0E7'
		},
		black: {
			default: "#000000",
			50: "#f6f6f6",
			100: "#d9d9d9",
			200: "#bfbfbf",
			300: "#a6a6a6",
			400: "#8c8c8c",
			500: "#737373",
			600: "#595959",
			700: "#404040",
			800: "#262626",
			900: "#0d0d0d",
		},
		white: {
			default: "#ffffff",
			50: "#f2f2f2",
			100: "#d9d9d9",
			200: "#bfbfbf",
			300: "#a6a6a6",
			400: "#8c8c8c",
			500: "#737373",
			600: "#595959",
			700: "#404040",
			800: "#262626",
			900: "#0d0d0d",
		},
		text: {
			get default() {
				return this[700];
			},
			50: "#fbf0f2",
			100: "#dcd8d9",
			200: "#bfbfbf",
			300: "#a6a6a6",
			400: "#8c8c8c",
			500: "#737373",
			600: "#595959",
			700: "#363636",
			800: "#282626",
			900: "#150a0d",
		},
		grey: {
			get default() {
				return this[300];
			},
			light: "#f9f9f9",
			50: "#fceff2",
			100: "#ddd7d9",
			200: "#c1bfbf",
			300: "#9c9c9c",
			400: "#8c8c8c",
			500: "#737373",
			600: "#595959",
			700: "#413f40",
			800: "#292526",
			900: "#16090d",
		},
		stroke: "#e1e1e1",
		backdrop: "#f9f9fb",
		checkbox: "#2f80ed",
		orange: "#ee732e",
		blue: "#4890e6",
	},

	gradients: {
		red: "linear-gradient(81.15deg, #e73f45 3.53%, #ea5970 93.86%)",
		lightblue: "linear-gradient(81.15deg, #4890e6 3.53%, #58a6ff 93.86%)",
		orange: "linear-gradient(81.15deg, #f29d38 3.53%, #ee732e 93.86%)",
		green: "linear-gradient(144deg, #a1e674 7%, #85ce82 60%, #85ce82 82%)",
	},

	fonts: {
		body: "Inter, system-ui, sans-serif",
		button: "Radikal, system-ui, sans-serif",
		footNote: "Inter, system-ui, sans-serif",
		h1: "Radikal, system-ui, sans-serif",
		h2: "Radikal, system-ui, sans-serif",
		h3: "Inter, system-ui, sans-serif",
		h4: "Radikal, system-ui, sans-serif",
		h5: "Inter, system-ui, sans-serif",
		label: "Inter, system-ui, sans-serif",
	},

	fontSizes: {
		body: UnitConvert.rem(17),
		button: UnitConvert.rem(16),
		footNote: UnitConvert.rem(15),
		label: UnitConvert.rem(12),
		h1: UnitConvert.rem(32),
		h2: UnitConvert.rem(28),
		h3: UnitConvert.rem(22),
		h4: UnitConvert.rem(20),
		h5: UnitConvert.rem(17),
	},

	space: [0, 0.25, 0.5, 1, 2, 4, 8, 16].map(Unit.rem),

	sizes: {
		max: "max-content",
		min: "min-content",
		full: "100%",
		app: "480px",
	},

	radii: {
		none: "0",
		xxs: Unit.px(LAYOUT_UNIT * 0.5),
		xs: Unit.px(LAYOUT_UNIT * 0.75),
		sm: Unit.px(LAYOUT_UNIT),
		md: Unit.px(22),
		lg: Unit.px(LAYOUT_UNIT * 2),
		xl: Unit.px(LAYOUT_UNIT * 4),
		rounded: "9999px",
	},

	zIndices: {
		hide: -1,
		auto: "auto",
		base: 0,
		docked: 10,
		sticky: 110,
		banner: 120,
		overlay: 130,
		modal: 140,
		popover: 150,
		skipLink: 160,
		toast: 170,
		tooltip: 180,
	},
	bgColors: [
		'#E8DCCF',
		'#F1C982',
		'#DCD5CC',
		'#D6A39E',
		'#EED5D8',
	]

});

export type SbTheme = typeof theme;
