import { LAYOUT_UNIT } from "./theme";

export class Unit {
  static rem = (amount: number) => amount + "rem";
  static em = (amount: number) => amount + "em";
  static pt = (amount: number) => amount + "pt";
  static px = (amount: number) => amount + "px";
}

export class UnitConvert {
  static rem = (px: number) => Unit.rem(round(px / LAYOUT_UNIT));
  static px = (rem: number) => Unit.rem(round(rem / LAYOUT_UNIT));
}

function round(n: number) {
  return Number(n.toFixed(3));
}
