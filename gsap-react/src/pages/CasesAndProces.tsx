import Navbar from "../components/hoc/Navbar/Navbar";
import HeaderLayout from "../components/layouts/HeaderLayout";
import PageLayout from "../components/layouts/PageLayout";
import CaseCard from "../components/molecules/CaseCard";

export default function CasesAndProces() {
  return (
    <>
      <Navbar />
      <PageLayout>
        <HeaderLayout
          h1="Cases & werkwijze"
          p="Geen proces is hetzelfde, we leggen graag aan de hand van een paar voorbeelden uit hoe wij tot een oplossing komen"
        />
        <div className="cards" style={{
          display: 'flex',
          flexDirection: 'column',
          rowGap: '6em'
        }}>
          <CaseCard
            to="/cases-en-werkwijze/rinkel" title="Rinkel" imgPath="/cases_rinkel.png"
            description="Middels animatie de eenvoud en waarde van Rinkel tonen en eventuele vragen direct weghalen bij de bezoeker."
            tagNames={['Concept', '2D animaties', 'Infographic Explainer']} />
          <CaseCard
            to="/cases-en-werkwijze/rinkel" title="ProRail" imgPath="/cases_prorail.png"
            description="ProRail vond in ons de ideale partner voor het ontwikkelen van verschillende middelen zoals explainers en infographics."
            tagNames={['Infographics', 'Explainer']} />
          <CaseCard
            to="/cases-en-werkwijze/rinkel" title="ESIG" imgPath="/cases_ESIG.png"
            description="ESIG (European Solvents Industry Group) is keen on promoting the safe and sustainable use of oxygenated and hydrocarbon solvents in europe."
            tagNames={['2D animaties', 'Explainer']} />
        </div>
      </PageLayout>
    </>
  )
}