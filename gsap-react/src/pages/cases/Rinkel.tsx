import gsap from "gsap";
import { useEffect, useRef } from "react";
import Navbar from "../../components/hoc/Navbar/Navbar";
import PageLayout from "../../components/layouts/PageLayout";
import FishLine from "../../components/molecules/FishLine";
import Tag from "../../components/molecules/Tag";

interface Props {
  h1: string;
  p: string;
}

export function CaseHeader(props: Props) {
  const myRef = useRef<HTMLTableHeaderCellElement>(null);

  // Fade in.
  useEffect(() => {
    myRef.current
    gsap.to(myRef.current, {
      opacity: 1,
      delay: 1
    })
  });

  const tags = ['2D Animation', 'Infographics', 'Explainers'].map(name => <Tag key={name} name={name} />);
  const Tags = () => <div style={{
    display: 'flex',
    columnGap: '15px',
    rowGap: '10px',
    padding: '20px 0',
    flexWrap: 'wrap',
    justifyContent: 'center'
  }}>
    {tags}
  </div>

  return <header
    ref={myRef}
    style={{
      maxWidth: '900px',
      textAlign: 'center',
      margin: 'auto',
      background: 'white',
      padding: '2em',
      marginBottom: '4em',
      opacity: 0
    }}>
    <h1 style={{ fontSize: '60px', fontWeight: '500', marginBottom: '25px' }}>{props.h1}</h1>
    <p style={{ color: '#363636', fontSize: '24px' }}>
      {props.p}
    </p>
    <Tags />
  </header>
}

export default function Rinkel() {

  const contentRef = useRef<HTMLDivElement>(null);
  const navbarRef = useRef<HTMLDivElement>(null);

  useEffect(() => {

    // Animate content up.
    const tl = gsap.timeline();
    tl.to(contentRef.current, {
      y: '70vw',
    });

    tl.to(contentRef.current, {
      y: 0,
      opacity: 1,
      delay: 2
    });

    // Animate navbar.
    navbarRef.current
    gsap.to(navbarRef.current, {
      opacity: 1,
      delay: 2.8
    })

  })

  return (
    <>
      <div ref={navbarRef} className="nav" style={{ opacity: 0 }}><Navbar /></div>
      <FishLine />
      <PageLayout paddingTop="0">
        <CaseHeader h1="Rinkel" p="Middels animate de eenvoud en waarde van Rinkel tonen en eventuele vragen direct weghalen bij de bezoeker." />
        <div ref={contentRef} className="content" style={{ opacity: 0 }}>
          <img src="/rinkel_detail.png" alt="Rinkel" style={{ width: '100%' }} />
        </div>
      </PageLayout>
    </>
  )
}