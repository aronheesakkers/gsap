import { theme } from "../assets/styles/theme"
import { Box } from "../components/atoms/Box"
import { Section } from "../components/atoms/Section"
import Navbar from "../components/hoc/Navbar/Navbar"
import PageLayout from "../components/layouts/PageLayout"

export default function Home() {
  return (
    <>
      <Navbar />
      <PageLayout>
        <Section backgroundColor={theme.bgColors[0]}>
          <div className="container">
            <h1>Rolling box</h1>
            <Box />
          </div>
        </Section>
        <Section backgroundColor={theme.bgColors[1]}>
          <div className="container">
            <h1>Section 2</h1>
            <Box />
          </div>
        </Section>
        <Section backgroundColor={theme.bgColors[2]}>
          <div className="container">
            <h1>Section 3</h1>
            <Box />
          </div>
        </Section>
        <Section backgroundColor={theme.bgColors[3]}>
          <div className="container">
            <h1>Section 4</h1>
            <Box />
          </div>
        </Section>

        <Section backgroundColor={theme.bgColors[4]}>
          <div className="container">
            <h1>Section 5</h1>
            <Box />
          </div>
        </Section>
      </PageLayout>
    </>
  )
}
