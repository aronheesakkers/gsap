import { Link, useLocation } from "@tanstack/react-location";
import gsap from "gsap";
import { ReactNode, useEffect, useRef } from "react";
import { theme } from "../../../assets/styles/theme";
import Button from "../../atoms/Button";
import Logo from "./logo";

type Color = "dark" | "light";

function NavLink({ children, href }: { children: ReactNode, href: string }) {
  return (
    <Link
      to={href}
      style={{
        listStyle: 'none',
        color: theme.colors.black[900]
      }}>
      {children}
    </Link>
  )
}

interface ColorScheme {
  backgroundColor: string,
  color: string
}

const schemeDark = {
  id: 1,
  color: theme.colors.white[100],
  backgroundColor: theme.colors.black[900]
}

const schemeLight = {
  id: 2,
  backgroundColor: 'transparant',
  color: theme.colors.black[900]
}

export default function Navbar({ color }: { color?: Color }) {
  const myRef = useRef(null);
  const schema: ColorScheme = color === 'dark' ? schemeDark : schemeLight;
  const location = useLocation();
  const onClick = () => location.navigate('/contact');

  useEffect(() => {
    gsap.fromTo(
      myRef.current,
      {
        display: "none",
        opacity: 0,
        marginTop: -50,
      },
      {
        display: "flex",
        opacity: 1,
        marginTop: 0,
        scrollTrigger: myRef.current,
      }
    );
  }, []);


  return (
    <nav ref={myRef} className="navbar" style={{
      display: 'flex',
      padding: theme.LAYOUT_PADDING,
      alignItems: 'center',
      justifyContent: 'space-between',
      height: '120px',
      backgroundColor: schema.backgroundColor,
      color: schema.color
    }}>

      <Logo />
      <ul className="links" style={{
        display: 'flex',
        columnGap: '50px',
        alignItems: 'center'
      }}>
        <NavLink href="/cases-en-werkwijze">Cases & werkwijze</NavLink>
        <NavLink href="/producten">Producten</NavLink>
        <NavLink href="/over-ons">Over ons</NavLink>
        <Button text="Aan de slag" onClick={onClick} />
      </ul>
    </nav>
  )
}