interface Props {
  h1: string;
  p: string;
}

export default function HeaderLayout(props: Props) {
  return (
    <header style={{ marginBottom: '6em' }}>
      <h1 style={{ marginBottom: '20px' }}>{props.h1}</h1>
      <p style={{ maxWidth: '400px', lineHeight: '30px' }}>{props.p}</p>
    </header>
  )
}
