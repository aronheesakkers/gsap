import { ReactNode } from 'react'
import { theme } from '../../assets/styles/theme'

export default function PageLayout({ children, paddingTop = '6em' }: { children: ReactNode, paddingTop?: string }) {
  return (
    <main style={{
      padding: theme.LAYOUT_PADDING, paddingTop: paddingTop
    }}>
      {children}
    </main>
  )
}
