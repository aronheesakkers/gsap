import { ReactNode } from "react";

export function Section(props: { backgroundColor: string, children: ReactNode }) {
  return (
    <section style={{ height: '100vh', padding: '4em', backgroundColor: props.backgroundColor }}>
      {props.children}
    </section>
  )
}