import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";
import { useEffect, useRef } from "react"

gsap.registerPlugin(ScrollTrigger);

export function Box() {
  const myRef = useRef<HTMLDivElement>(null);

  function listener() {
    if (!myRef.current) return;
    const currentValue = myRef.current.getAttribute("data-on");
    const isOn = currentValue === "true";
    const newValue = isOn ? "false" : "true";

    isOn
      ? gsap.to(myRef.current, {
        x: 0,
        rotate: -180,
      })
      : gsap.to(myRef.current, {
        x: 200,
        rotate: 180,
      });
    myRef.current.setAttribute("data-on", newValue);
  }

  useEffect(() => {
    gsap.fromTo(
      myRef.current,
      {
        scrollTrigger: myRef.current,
        opacity: 0,
        y: -60,
      },
      {
        y: 0,
        opacity: 1,
        scrollTrigger: {
          trigger: myRef.current,
          scrub: true,
        },
      }
    );

    const current = myRef.current;
    if (current) {
      current.addEventListener("click", listener);
    }

    return () => {
      myRef.current?.removeEventListener('click', listener);
    }
  }, [])


  return (
    <div ref={myRef} className="box" data-on="false"
      style={{
        backgroundColor: "black",
        height: '120px',
        width: '120px',
        borderRadius: '20px'
      }}
    />
  )
}