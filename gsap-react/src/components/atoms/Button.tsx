import { theme } from "../../assets/styles/theme";

interface Props {
  text: string;
  onClick: () => void;
}

export default function Button(props: Props) {
  return (
    <div
      style={{
        backgroundColor: theme.colors.primary[600],
        padding: '10px 40px',
        color: 'white',
        fontWeight: '600',
        borderRadius: '5px'
      }}
      onClick={props.onClick}>
      {props.text}
    </div>
  )
}
