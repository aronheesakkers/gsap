import gsap, { Power4 } from "gsap";
import { useEffect, useRef } from "react"
import { theme } from "../../assets/styles/theme";
gsap.registerPlugin()

export default function FishLine() {
  const myRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const t1 = gsap.timeline();
    // t1.to(myRef, { height: '100' });
    // t1.to(myRef, { height: '100vh' });
    // t1.to(myRef, { height: '70vh' });
    // t1.to(myRef, { height: '100' });

    t1.to(myRef.current, { height: '130vh', delay: 1, duration: '0.8', ease: Power4.easeOut });
    t1.to(myRef.current, { height: '600px', delay: .7, ease: Power4.easeOut });
    t1.to(myRef.current, { height: '590px', delay: 0.5, duration: 0.2, ease: Power4.easeOut });
  })

  return <div
    ref={myRef}
    style={{
      position: 'absolute',
      height: '200px',
      zIndex: -1,
      width: '100%',
      top: '-150px',
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center'
    }}>
    <div style={{
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center'
    }}>

      <div style={{
        height: '100%',
        width: '1px',
        background: '#D9D9D9',
        marginLeft: '1.5px',
      }} />
      <svg width="22" height="17" viewBox="0 0 22 17" fill={theme.colors.primary[600]} xmlns="http://www.w3.org/2000/svg">
        <path d="M11.7289 12.1336C12.2292 14.368 13.7007 15.7822 16.32 15.7822L16.4377 15.7822C19.2924 15.7822 22 14.1135 22 8.76788L22 5.51528L16.938 5.51528L16.938 6.95774C16.938 8.17393 16.4377 8.62646 15.496 8.62646L15.3783 8.62646C14.4365 8.62646 14.0245 8.14565 14.0245 6.95774L14.0245 -3.48621e-07L9.46283 -5.48017e-07L9.46283 6.78804C9.46283 8.20221 8.8448 8.7396 7.78532 8.7396L7.6676 8.7396C6.60812 8.7396 5.96066 8.14564 5.96066 6.87289L5.96066 5.54356L0.869274 5.54356L0.869274 8.45676C0.869274 14.0003 3.04709 16.2347 6.75527 16.2347L6.87299 16.2347C9.87485 16.1782 11.3758 14.368 11.7289 12.1336Z" fill="#FE5143" />
      </svg>
    </div>
  </div >
}