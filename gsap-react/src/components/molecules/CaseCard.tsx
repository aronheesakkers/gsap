import CheckIt from './CheckIt';
import Tag from './Tag';

interface Props {
  tagNames: string[],
  title: string,
  description: string,
  imgPath: string,
  to: string
}

export default function CaseCard(props: Props) {
  const tags = props.tagNames.map(name => <Tag key={name} name={name} />);
  const Tags = () => <div style={{
    display: 'flex',
    columnGap: '15px',
    rowGap: '10px',
    padding: '20px 0',
    flexWrap: 'wrap'
  }}>
    {tags}
  </div>;

  return (
    <div style={{ display: 'flex', columnGap: '6em', alignItems: 'center' }}>
      <img src={props.imgPath} alt='case rinkel' width="450" style={{ height: '650px', objectFit: 'cover' }} />
      <div className="content" style={{ maxWidth: '700px' }}>
        <h2>{props.title}</h2>
        <Tags />
        <p style={{ fontSize: '22px', lineHeight: '40px' }}>{props.description}</p>
        <CheckIt to={props.to} />
      </div>
    </div>
  )
}
