import { theme } from "../../assets/styles/theme";

interface Props {
  name: string;
}

export default function Tag(props: Props) {
  return (
    <div style={{
      backgroundColor: theme.colors.tertiary.default,
      padding: '5px 15px',
      fontSize: '12px',
      borderRadius: '50px'
    }}>{props.name}</div>
  )
}
