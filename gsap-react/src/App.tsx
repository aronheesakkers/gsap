import { Outlet, ReactLocation, Router } from '@tanstack/react-location'
import { routes } from './routes'
const reactLocation = new ReactLocation();

function App() {
  return (
    <>
      <Router routes={routes} location={reactLocation}>
        <main style={{ position: 'relative' }}>
          <Outlet />
        </main>
      </Router>
    </>
  )
}

export default App