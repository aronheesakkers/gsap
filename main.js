import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
gsap.registerPlugin(ScrollTrigger);

function onLoad() {
  const boxes = Array.from(document.getElementsByClassName("box"));

  boxes.forEach((box) => {
    // Entering.
    gsap.fromTo(
      box.parentElement,
      {
        scrollTrigger: box,
        opacity: 0,
        y: -50,
      },
      {
        y: 0,
        opacity: 1,
        scrollTrigger: {
          trigger: box,
          scrub: true,
          onLeave: (self) => {},
        },
      }
    );

    box.addEventListener("click", () => {
      const currentValue = box.getAttribute("data-on");
      const isOn = currentValue === "true";
      const newValue = isOn ? "false" : "true";

      isOn
        ? gsap.to(box, {
            x: 0,
            rotate: -180,
          })
        : gsap.to(box, {
            x: 200,
            rotate: 180,
          });
      box.setAttribute("data-on", newValue);
    });
  });
}

window.addEventListener("load", onLoad);
